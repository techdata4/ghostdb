import subprocess


def execute_sql_script(sql_file_path):
    config_file = "config/my.cnf"
    try:
        command = f"mysql --defaults-extra-file={config_file} < {sql_file_path}"
        subprocess.run(command, shell=True, check=True)
        print(f"Le script SQL {sql_file_path} a été exécuté avec succès.")
    except subprocess.CalledProcessError as e:
        print(f"Erreur lors de l'exécution du script SQL: {e}")


if __name__ == "__main__":
    path_to_sql_file = "create_database.sql"
    execute_sql_script(path_to_sql_file)
