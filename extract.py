import configparser
import logging
from functools import wraps
import requests
from bs4 import BeautifulSoup
import mysql.connector
from mysql.connector import Error


def setup_logger():
    """

    Configure la journalisation avec la configuration spécifiée.

    :retourne: L'instance du journal avec la configuration spécifiée.
    :rtype: logging.Logger
    """
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    return logging.getLogger(__name__)


logger = setup_logger()


def get_db_config(config_file_path):
    """
    :param config_file_path: le chemin du fichier de configuration contenant les détails de connexion à la base de données.
    :return: un dictionnaire contenant les valeurs de configuration de la base de données.

    Cette méthode lit le fichier de configuration spécifié par `config_file_path`
    et récupère les valeurs de configuration de la base de données. Il renvoie un dictionnaire
    avec les clés suivantes:
    - ``host``: L'hôte du serveur de base de données.
    - ``user``: Le nom d'utilisateur utilisé pour se connecter à la base de données.
    - ``password``: Le mot de passe utilisé pour se connecter à la base de données.
    - ``database``: Le nom de la base de données à laquelle se connecter.
    """
    config = configparser.ConfigParser()
    config.read(config_file_path)
    db_config = {
        "host": config["client"]["host"],
        "user": config["client"]["user"],
        "password": config["client"]["password"],
        "database": config["client"]["database"],
    }
    return db_config


def connect_to_db(config_file_path):
    """
    :parameter config_file_path: Le chemin du fichier de configuration contenant les détails de connexion à la base de données.
    :param config_file_path: The file path to the configuration file containing the necessary database connection details.
    :return: L'objet de connexion à la base de données si la connexion réussit, sinon None.
 
 
    Se connecte à la base de données en utilisant le chemin du fichier de configuration fourni.
    Récupère la configuration de la base de données à partir du
    fichier de configuration en utilisant la fonction `get_db_config`. Tente d'établir une connexion à la base de données en utilisant la
    configuration récupérée. Si la connexion réussit,
     un message de journalisation indiquant la connexion réussie est enregistré, et l'objet de connexion est renvoyé.
    Si une erreur se produit pendant la tentative de connexion, un message de journalisation d'erreur est enregistré avec les détails de l'erreur, et None est renvoyé.

    """
    db_config = get_db_config(config_file_path)
    try:
        connection = mysql.connector.connect(**db_config)
        logger.info("Successfully connected to database.")
        return connection
    except Error as e:
        logger.error(f"Database connection error: {e}")
        return None


def close_db_connection(connection):
    """
    :param connection: L'objet de connexion à la base de données.
    :return: None.
    
    """
    if connection and connection.is_connected():
        connection.close()
        logger.info("Database connection closed.")


def logger_decorator(func):
    """
    Decorator for logging the execution of a function.
    Décorateur pour enregistrer l'exécution d'une fonction.

    :param func: La fonction a décoré.
    :return: L fonction décorée.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        logger.info(f"Finished: {func.__name__} with result: {result}")
        return result

    return wrapper


def get_html_content(url):
    """
    Function to retrieve the HTML content from a given URL.
    Fonction pour récupérer le contenu HTML à partir d'une URL donnée.
        

    :param url: L'URL de la page à récupérer.
    :type url: Str
    :retourne: Le contenu HTML de la page.
    :rtype: Str ou None
    """
    try:
        response = requests.get(url, timeout=1)
        response.raise_for_status()
        return response.text
    except requests.exceptions.RequestException as e:
        logger.error(f"Error fetching the page: {e}")
        return None


@logger_decorator
def extract_node_one(html_content, base_url, selectors):
    """Extracts the first node from HTML content based on given selectors.
    Extrait le premier nœud du contenu HTML en fonction des sélecteurs donnés.

    :parameter html_content: Le contenu HTML à partir duquel extraire le nœud.
    :param base_url: L'URL de base à utiliser pour construire les URL de région.
    :param selectors: Un dictionnaire de catégorie et son sélecteur.

    :return: A dictionary containing extracted data, where category is the key and a list of tuples containing element text and region URL is the value.
:
    """
    soup = BeautifulSoup(html_content, "html.parser")
    extracted_data = {}
    for category, selector in selectors.items():
        elements = soup.select(selector)
        links = []
        for element in elements:
            link_element = element.find_next_sibling("a")
            if link_element and link_element.has_attr("href"):
                region_url = base_url + link_element["href"]
                links.append((element.text.strip(), region_url))
        extracted_data[category] = links
    return extracted_data


@logger_decorator
def extract_node_two(connection, base_url, data_one):
    """
    Extracts data from data_one and returns extracted_data_two.
    Extrait les données de data_one et renvoie extracted_data_two.

    :param connection: The connecti

    :param connection: The connection to the database.
    :param base_url: The base URL for the website.
    :param data_one: The data to be extracted.
    :return: The extracted data.
    """
    extracted_data_two = {}
    for category_name, data_list in data_one.items():
        parent_id = get_category_id_by_name(connection, category_name)

        for data in data_list:
            if data and len(data) == 2:
                name, url = data
                page_content = get_html_content(base_url + url)
                if page_content:
                    soup = BeautifulSoup(page_content, "html.parser")
                    places_of_note = soup.find_all(class_='w3-row-padding w3-left-align w3-margin-top w3-card')
                    for place in places_of_note:
                        place_name = place.h4.text if place.h4 else 'Unknown'
                        place_url = place.find('a')['href'] if place.find('a') else None
                        if "Places of Note" not in extracted_data_two:
                            extracted_data_two["Places of Note"] = []
                        extracted_data_two["Places of Note"].append((place_name, base_url + place_url if place_url else ''))
                        logger.info(f"Extracted place of note: {place_name} from {category_name}")
        break
    return extracted_data_two

def get_category_id_by_name(connection, name):
    """
    Interroge la base de données pour récupérer l'ID de la catégorie pour un nom de catégorie donné.

    :param connection: L'objet de connexion à la base de données.
    :type connection: object
    :param name: Le nom de la catégorie.
    :type name: str
    :return: L'ID de la catégorie si trouvé, None sinon.
    :rtype: int ou None

    """
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT category_id FROM ghost.category WHERE name = %s", (name,))
        result = cursor.fetchone()
        if result:
            return result[0]
    except Error as e:
        logger.error(f"Error retrieving category ID for {name}: {e}")
    finally:
        cursor.close()
    return None

@logger_decorator
def extract_data_from_links(urls, selectors):
    """
    Extrait les données d'une liste d'URL en utilisant des sélecteurs CSS.

    :param urls: Une liste d'URL à partir desquelles extraire des données.
    :type urls: list[str]

    :param selectors: Une liste de sélecteurs CSS pour correspondre aux éléments dans le contenu HTML.
    :type selectors: list[str]

    :return: Un dictionnaire contenant des données extraites pour chaque sélecteur.
    :rtype: dict[str, list[str]]
    """
    extracted_data = {}
    for url in urls:
        # Fetch and parse the HTML content of the page
        html_content = get_html_content(url)
        if html_content is None:
            continue
        soup = BeautifulSoup(html_content, "html.parser")

        for selector in selectors:
            elements = soup.select(selector)
            for element in elements:
                # Add the extracted data to the dictionary
                if selector not in extracted_data:
                    extracted_data[selector] = []
                extracted_data[selector].append(element.text.strip())

    return extracted_data


@logger_decorator
def determine_parent_name(connection, name, category_type):
    """
    :param connection: The database connection object.
    :param name: The name of the category.
    :param category_type: The type of the category.
    :return: The name of the parent category, or None if no parent category found.

    """
    cursor = connection.cursor()
    try:
        # Query the category table to find the parent of the given category
        cursor.execute(
            "SELECT parent FROM ghost.category WHERE name = %s AND category_type = %s",
            (name, category_type),
        )
        result = cursor.fetchone()
        if result:
            parent_id = result[0]
            if parent_id is not None:
                cursor.execute(
                    "SELECT name FROM ghost.category WHERE category_id = %s",
                    (parent_id,),
                )
                result = cursor.fetchone()
                if result:
                    return result[0]
    except mysql.connector.Error as e:
        logger.error(f"Error fetching parent category: {e}")
    finally:
        cursor.close()
    return None


@logger_decorator
def extract_node_two(connection, base_url, data_one):
    """
    Extracts data from data_one and returns extracted_data_two.

    :param connection: The connection object used for data extraction.
    :param base_url: The base URL used for constructing full URLs.
    :param data_one: A dictionary containing data for extraction.
    :return: A dictionary containing extracted data.
    """
    extracted_data_two = {}
    for category_name, data_list in data_one.items():
        for data in data_list:
            if data and len(
                    data) == 2:  # ensure data exists and contains exactly two items
                name, url = data
                # Only append base_url if it's not already in the url
                full_url = url if base_url in url else base_url + url
                page_content = get_html_content(full_url)
                if page_content:
                    soup = BeautifulSoup(page_content, "html.parser")
                    places_of_note = soup.find_all(
                        class_='w3-row-padding w3-left-align w3-margin-top w3-card')
                    for place in places_of_note:
                        place_name = place.h4.text if place.h4 else 'Unknown'
                        place_url = place.find(
                            'a')['href'] if place.find('a') else None
                        if "Places of Note" not in extracted_data_two:
                            extracted_data_two["Places of Note"] = []
                        extracted_data_two["Places of Note"].append(
                            (place_name, base_url + place_url if place_url else ''))
    return extracted_data_two
@logger_decorator
def extract_child_data(soup, selectors):
    """
    Extracts child data from soup using the given selectors.

    :param soup: The soup object containing the HTML content.
    :type soup: BeautifulSoup

    :param selectors: A dictionary of category types and their corresponding CSS selectors.
    :type selectors: dict

    :return: A list of child category data, where each entry is a tuple containing the category type, name, and URL.
    :rtype: list
    """
    child_categories = []
    for category_type, selector in selectors.items():
        for element in soup.select(selector):
            name_elements = element.find_all('h4')  # Supposons que les noms des sous-catégories sont dans des balises <h4>
            url_elements = element.find_all('a')  # Les URLs sont toujours dans des balises <a>
            for element_name, url_elemnt in zip(name_elements, url_elements):
                name = element_name.text.strip()
                url = url_elemnt['href']
                child_categories.append((category_type, name, url))
    return child_categories


@logger_decorator
def set_category_type_name(name):
    """
    Set the category type name based on the given name.

    :param name: The name to determine the category type from.
    :return: The determined category type.

    """
    normalized_name = name.lower()

    if "report" in normalized_name or name == "Reports: Browse People":
        category_type = "reports"
    elif "calendar" in normalized_name:
        category_type = "calendar"
    elif "subsubgeoggrapic" in normalized_name:
        category_type = "subsubgeoggrapic"
    elif "subgeographic" in normalized_name:
        category_type = "subgeographic"
    else:
        category_type = "geographic"

    return category_type


@logger_decorator
def insert_category(connection, name, category_type, parent_name=None):
    """
    :param connection: L'objet de connexion à la base de données
    :param name: Le nom de la catégorie à insérer
    :param category_type: Le type de la catégorie (par exemple, 'nourriture', 'électronique', etc.)
    :param parent_name: Le nom de la catégorie parente, le cas échéant (par défaut est None)
    :return: L'ID de la nouvelle catégorie insérée ou None si une erreur s'est produite

    Cette méthode insère une nouvelle catégorie dans la base de données.
     Si une catégorie parente est spécifiée, la méthode détermine l'ID de la catégorie parente. Si la catégorie parente n'existe pas, un
    * message d'erreur est enregistré et None est renvoyé.

    La méthode vérifie ensuite si une catégorie avec le même nom,
     type et parent existe déjà dans la base de données. Si elle existe, l'ID de la catégorie existante est renvoyé.

    Si la catégorie n'existe pas, la méthode l'insère dans la base de données et renvoie l'ID de la nouvelle catégorie insérée.

    Si une erreur se produit pendant l'exécution de la méthode, un message d'erreur est enregistré et None est renvoyé.


    Exemple d'utilisation :
        connection = obtenir_connexion_base_de_donnees()
        id_categorie = inserer_categorie(connection, 'Téléphones mobiles', 'électronique', 'Électronique')
    """
    cursor = connection.cursor()

    # Déterminez l'ID du parent si un parent est spécifié
    parent_id = None
    if parent_name:
        try:
            cursor.execute(
                "SELECT category_id FROM ghost.category WHERE name = %s", (parent_name,))
            parent_result = cursor.fetchone()
            if parent_result:
                parent_id = parent_result[0]
            else:
                logger.error(f"Parent category '{parent_name}' not found.")
                return None
        except mysql.connector.Error as e:
            logger.error(f"Error fetching parent category: {e}")
            return None

    try:
        cursor.execute(
            "SELECT category_id FROM ghost.category"
            " WHERE name = %s AND category_type = %s AND"
            " (parent = %s OR parent IS NULL AND %s IS NULL)",
            (name, category_type, parent_id, parent_id),
        )
        existing_category = cursor.fetchone()
        if existing_category:
            logger.info(
                f"Category '{name}' of type '{category_type}' already exists with ID: {existing_category[0]}."
            )
            return existing_category[0]
        else:
            # Insérez la nouvelle catégorie si elle n'existe pas
            cursor.execute(
                "INSERT INTO ghost.category (name, category_type, parent) VALUES (%s, %s, %s)",
                (name, category_type, parent_id),
            )
            connection.commit()
            new_category_id = cursor.lastrowid
            logger.info(
                f"Category '{name}' of type '{category_type}' inserted successfully with ID: {new_category_id}."
            )
            return new_category_id
    except mysql.connector.Error as e:
        logger.error(f"Error inserting new category: {e}")
        return None
    finally:
        cursor.close()


def main(config_file_path, base_url, selectors_node_one, selector_node_two):
    """
    :param config_file_path: Le chemin du fichier de configuration utilisé pour établir la connexion à la base de données.
    :param base_url: L'URL de base utilisée pour le scraping des données.
    :param selectors_node_one: Les sélecteurs utilisés pour extraire les données du premier nœud du contenu HTML.
    :param selector_node_two: Le sélecteur utilisé pour extraire les données du deuxième nœud du contenu HTML.
    :return: Aucun

    La fonction `main` est le point d'entrée du programme.
     Elle récupère le contenu HTML d'une URL spécifiée, extrait les données des premier et deuxième nœuds du contenu HTML,
      et insère
    * les données extraites dans une base de données.

    La fonction prend les paramètres suivants :
    - `config_file_path` (str): Le chemin du fichier de configuration utilisé pour établir la connexion à la base de données.
    - `base_url` (str): L'URL de base utilisée pour le scraping des données.
    - `selectors_node_one` (list[str]): Les sélecteurs utilisés pour extraire les données du premier nœud du contenu HTML.
    - `selector_node_two` (str): Le sélecteur utilisé pour extraire les données du deuxième nœud du contenu HTML.

    La fonction ne renvoie aucune valeur.

    Exemple d'utilisation :
    ```
    main("config.txt", "https://example.com", ["selector1", "selector2"], "selector3")
    ```
    """

    allowed_category_types = ["geographic", "reports", "calendar", "people"]
    logger.info("Starting main function")
    connection = connect_to_db(config_file_path)
    if not connection:
        logger.error("Failed to establish database connection.")
        return

    visited_links = set()
    print("Visited links: ", visited_links)

    html_content = get_html_content(base_url)
    if html_content:
        extracted_data_one = extract_node_one(
            html_content, base_url, selectors_node_one)
        extracted_data_two = extract_node_two(
            connection, base_url, extracted_data_one)
        extracted_data = {**extracted_data_one, **extracted_data_two}
        print("Extracted data: ", extracted_data)
        for category_name, data in extracted_data.items():
            category_type = set_category_type_name(category_name)
            if category_type in allowed_category_types:
                for name, url in data:
                    page_content = get_html_content(url)
                    if page_content:
                        soup = BeautifulSoup(page_content, 'html.parser')
                        child_data = extract_child_data(soup, selector_node_two)
                        print("Child data: ", child_data)
                        for child_category_type, child_name, child_url in child_data:
                            if child_category_type in allowed_category_types:
                                insert_category(connection, child_name, child_category_type)
            else:
                logger.error(f"Category type '{category_type}' is not allowed for '{category_name}'.")
    else:
        logger.error("Failed to retrieve HTML content.")


if __name__ == "__main__":
    CONFIG_FILE_PATH = "config/my.cnf"
    BASE_URL = "http://www.paranormaldatabase.com/"
    selector_node_one = {
        "ENGLAND": "div.w3-row-padding.w3-left-align.w3-margin-top.w3-card h4",
        "Scotland & Wales": "div.w3-row-padding.w3-center.w3-margin-top h3",
        "NI RI OtherR": "div.w3-row-padding.w3-center.w3-margin-top:nth-of-type(2) h3",
        "Paranormal Calendar": "div.w3-row:nth-child(8) > div:nth-child(2) h5",
        "Reports and Places": "div.w3-row:nth-child(6) > div:nth-child(-n+3)",
        "The Paranormal Calendar": "div.w3-half.w3-container.w3-border-left.w3-left-align h5",
        "Reports: Browse People": "div.w3-quarter.w3-container.w3-border-left.w3-left-align h5",
    }

    selector_node_two = {
        "Places of Note": "div.w3-row-padding.w3-left-align.w3-margin-top.w3-card",
        "County": "div.w3-row-padding.w3-left-align.w3-margin-top.w3-card",
    }


    selector_node_4= {
        "Cases": ""
    }
    main(CONFIG_FILE_PATH, BASE_URL, selector_node_one, selector_node_two)
