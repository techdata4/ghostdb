DROP DATABASE IF EXISTS ghost;
CREATE DATABASE ghost;
USE ghost;

CREATE TABLE element
(
    element_id   INT AUTO_INCREMENT PRIMARY KEY,
    title        VARCHAR(50) NOT NULL,
    location     VARCHAR(50) NOT NULL,
    element_type VARCHAR(50) NOT NULL,
    temporality  VARCHAR(50) NOT NULL,
    comments     TEXT,
    month        INT,
    weather      BOOLEAN,
    UNIQUE(title, location, element_type, temporality)
);

CREATE TABLE category
(
    category_id   INT AUTO_INCREMENT PRIMARY KEY,
    parent        INT                                            NULL,
    name          VARCHAR(255)                                   NOT NULL,
    category_type ENUM('geographic', 'subgeographic', 'subsubgeoggrapic', 'reports', 'calendar', 'people') NOT NULL
);

CREATE TABLE element_category
(
    element_id  INT,
    category_id INT,
    PRIMARY KEY(element_id, category_id),
    FOREIGN KEY(element_id) REFERENCES element(element_id),
    FOREIGN KEY(category_id) REFERENCES category(category_id)
);
